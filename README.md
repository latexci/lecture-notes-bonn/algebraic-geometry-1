# Algebraic Geometry 

These are the lecture notes for the 'Algebraic Geometry', taught in winter term 22/23 at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-geometry-1/2022_Algebraic_Geometry.pdf
[2]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-geometry-1/2022_Algebraic_Geometry.log
[3]: https://latexci.gitlab.io/lecture-notes-bonn/algebraic-geometry-1/
