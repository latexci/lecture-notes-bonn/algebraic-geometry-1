\lecture[Presheaves. Sheaves. Morphisms of (pre)sheaves. Structure sheaf.]{Mo 10 Okt 2022}{Presheaves and Sheaves}

\section{Quick motivation}

In this course, we study \vocab{schemes}.
These are objects that \enquote{locally look like} $(\Spec A, A)$.
We will spend roughly three weeks to make this notion precise.
Examples include $\Spec A$ itself, so commutative algebra will be a vital part
of algebraic geometry.

In classical algebraic geometry, you learn to now the affine and projective varieties
$\mathbb{A}_k^n$ and $\mathbb{P}_k^n$.
These are in fact the closed points of similarly defined schemes,
so classical algebraic geometry is in fact a subset of modern algebraic geometry.

\section{Presheaves and Sheaves}

\begin{definition}[Presheaf]
  \label{def:presheaf}
  Let $X$ be a topological space and let $\mathcat{C}$ be a category.
  A \vocab{presheaf} $\mathcal{F}$ of $\mathcat{C}$ on $X$
  consists of
  \begin{enumerate}[1)]
    \item For all open $U\subset X$, an object $\mathcal{F}(U)$ in $\mathcat{C}$.
    \item For all open $V\subset U\subset X$, a morphism
      $\rho_{U, V} \colon  \mathcal{F}(U) \to \mathcal{F}(V)$.
  \end{enumerate}
  such that for all open $W\subset V\subset U\subset X$,
  the restriction map $\rho_{U,U}$ is the identity $\id_U$
  and the restrictions are compatible, that is,
  $\rho_{V,W} \circ \rho_{U,V} = \rho_{U,W}$.
\end{definition}

\begin{remark}
  We also say that $\mathcal{F}$ is a sheaf on $X$ with values in $\mathcat{C}$.
  As we will usually take $\mathcat{C}$ to be something like sets,
  abelian groups, rings, etc, we will also speak of
  \enquote{a presheaf \emph{of} abelian groups on $X$}.
  Notice that these are concrete categories, so we can speak of elements
  of $\mathcal{F}(U)$ (and you should think about $\mathcal{F}(U)$ in this way).
\end{remark}

\begin{remark}
  Elements of $\mathcal{F}(U)$ are called \vocab{sections of $\mathcal{F}$ over $U$},
  similarly $\mathcal{F}(U)$ is the \vocab{space of sections} of $\mathcal{F}$ over $U$.
  The elements of $\mathcal{F}(X)$ are called the \vocab{global sections}.

  We will also use the notation $\Gamma(U, \mathcal{F})$ for $\mathcal{F}(U)$.
  Sometimes these will also be written as $H^0(U, \mathcal{F})$,
  suggesting that the section functor can actually be derived to consider
  cohomology groups, but this only be part of Algebraic Geometry II.

  The maps $\rho_{U,V}$ are called \vocab{restriction maps}. 
  For $s\in \mathcal{F}(U)$, we write $\frestriction{s}{V} \coloneqq \rho_{U,V}(s)$
  for the \vocab{restriction of $s$ to $V$}. 
\end{remark}

\begin{example}
  \begin{enumerate}[(1)]
    \item 
    For any object $A$ in $\mathcat{C}$,
    there is the \vocab{constant presheaf} $\underline{A}'$
    with $\underline{A}'(U) = A$ and $\rho_{U,V} = \id$.

    Notice that we use a \enquote{$'$} here to distinguish this from the
    constant sheaf, which we will define at some later point in the lecture.
    \item
      The \vocab{presheaf of continuous functions}, denoted $\mathcal{C}^0$,
      is the sheaf given by
      \[
        \mathcal{C}^0(U) \coloneqq
        \set{ f\colon U \to  \mathbb{R} \suchthat \text{$f$ is continuous} } 
      \]
      where $\rho_{U,V}$ is given by function restriction.
    \item More generally, if $\pi\colon Y \to  X$ is continuous,
      there is the presheaf $\mathcal{F}_{π}$ of continuous sections of $π$.
      It is defined by
      \[
        \mathcal{F}_π(U) \coloneqq
        \set{ s\colon U \to Y \suchthat \text{$s$ is continuous, $\pi \circ s = \id$ }} 
      \]
      This example is actually universal in a certain sense,
      as we will see in Exercise $5$.
  \end{enumerate}
\end{example}

\begin{remark}
   There is also a different viewpoint on how to define a presheaf.
   For a topological space $X$, let $\Ouv(X)$ be the category of all open subsets
   of $X$, where the maps are the inclusions of open sets, that is,
   \[
     \Mor(U,V) \coloneqq \begin{cases}
       \emptyset & \text{if $U \not \subset V$} \\
       ι\colon U \hookrightarrow V & \text{if $U \subset V$}
       .
     \end{cases}
   \]
   Then, a presheaf $\mathcal{F}$ of $\mathcat{C}$ on $X$ is just
   a (contravariant) functor $\Ouv_X^{\op} \to \mathcat{C}$.
\end{remark}

\begin{definition}
  A morphism $\varphi \colon \mathcal{F} \to \mathcal{G}$ of presheaves on $X$
  consists of a collection of morphisms
  $\varphi (U) \colon \mathcal{F}(U) \to \mathcal{G}(U)$
  for all open $U\subset X$ such that they are compatible with restriction maps,
  that is,
  for any $V \subset U$, the diagram
  \[
    \begin{tikzcd}
      \mathcal{F}(U)
      \ar[swap]{d}{\rho_{\mathcal{F}, U, V}}
      \ar{r}{\varphi (U)}
      &
      \mathcal{G}(U)
      \ar{d}{\rho_{\mathcal{G}, U, V}}
      \\
      \mathcal{F}(V)
      \ar[swap]{r}{\varphi (V)}
      &
      \mathcal{G}(V)
    \end{tikzcd}
  \]
  commutes.
\end{definition}

\begin{trivial}
  With the viewpoint of presheafes as functors $\Ouv_X^{\op} \to \mathcat{C}$,
  a morphism of presheafes is just a natural transformation of functors.
\end{trivial}

\begin{example}
  \begin{enumerate}[1.]
    \item 
      Every morphism $A \to B$ in $\mathcat{C}$ yields a morphism
      $\underline{A}' \to \underline{B}'$.
    \item
      If $X = \mathbb{R}^n$,
      then $\mathcal{C}^{\infty}(U)$ is defined on sections by
      $C^{\infty}(U) \coloneqq \allowbreak
      \set{ f\colon U \to  \mathbb{R}^n \suchthat \text{$f$ smooth} } $
      and the functions restriction.
      Then, the inclusions $C^{\infty}(U) \to C^0(U)$
      give rise to a morphism of presheaves
      $\mathcal{C}^{\infty}(U) \to \mathcal{C}^0(U)$.
    \item
      If $Y_2 \xrightarrow{\pi_2}
      Y_1 \xrightarrow{\pi_1}
      X$ are continuous,
      we get a morphism of presheaves
        \begin{equation*}
        \varphi : \left| \begin{array}{c c l} 
        \mathcal{F}_{π_1 \circ π_2} & \longrightarrow & \mathcal{F}_{π_1} \\
        s & \longmapsto &  π_1 \circ s
        .
        \end{array} \right.
      \end{equation*}
  \end{enumerate}
\end{example}

\begin{remark}
  There is an equivalence of categories between the category of presheaves on $X$
  and the functor category $\Fun(\Ouv_X^{\op}, \mathcat{C})$.
\end{remark}

\begin{definition}
  Let $\mathcat{C}$ be one of the categories $\Set$, $\Ab$, $\Ring$.
  A \vocab{sheaf $\mathcal{F}$ of $\mathcat{C}$ on $X$}  is a presheaf such that
  for all open $U\subset X$ and all open coverings $U = \bigcup_{i \in  I} U_i$,
  we have the following:
  \begin{enumerate}[h]
    \item For all $s$, $t \in \mathcal{F}(U)$,
      if $\frestriction{s}{U_i} = \frestriction{t}{U_i}$ for all $i\in I$,
      then $s=t$.
    \item For all $\set{ s_i } _{i\in I}$ with $s_i \in \mathcal{F}(U_i)$,
      such that $\frestriction{s_i}{U_i \cap U_j} = \frestriction{s_j}{U_i \cap U_j}$
      for all $i$, $j\in I$, then there exists an $s\in \mathcal{F}(U)$
      such that $\frestriction{s}{U_i} = s_i$ for all $i\in I$.
  \end{enumerate}
\end{definition}

\begin{notation}
  Condition $(i)$ is called the \enquote{locality} condition.
  Condition $(ii)$ is called the \enquote{gluing} condition. 
\end{notation}

\begin{remark}
  \begin{enumerate}[1.]
    \item 
      The section $s$ that exists by condition $(ii)$ is unique by $(i)$.
    \item
      More generally, if $\mathcat{C}$ has products,
      then a presheaf is called a sheaf if
      \[
        \begin{tikzcd}[column sep = huge]
          \mathcal{F}(U)
          \ar{r}{\rho_{U, U_i}}
          &
          \prod_{i \in I} \mathcal{F}(U_i)
          \ar[shift left]{r}{\prod \rho_{U_i, U_i \cap U_j}}
          \ar[shift right, swap]{r}{\prod \rho_{U_j, U_i \cap U_j}}
          &
          \prod_{i,j \in I} \mathcal{F}(U_i \cap U_j)
        \end{tikzcd}
      \]
      is an equalizer diagram.
  \end{enumerate}
\end{remark}

\begin{example}
  \begin{enumerate}[1.]
    \item 
      If $\mathcal{F}$ is a sheaf (of sets, \ldots),
      let $U = \emptyset\subset X$ and $I = \emptyset$.
      Then, any two sections $s$, $s' \in \mathcal{F}(\emptyset)$ agree
      on the restrictions to the $U_i$ (since there is no $U_i$ at all),
      so they must be equal.
      We deduce that $\mathcal{F}(\emptyset)$ has at most one element.
    \item
      $\mathcal{C}^0$ (and $\mathcal{C}^{\infty}$ if $X = \mathbb{R}^n$)
      are sheaves, since for all $U \subset X$ open
      \begin{enumerate}[(i)]
        \item Two functions $f,g \colon U \to \mathbb{R}$ that coincide on an
          open cover are equal (since they coincide in every point).
        \item Given an open cover $U = \bigcup_{i \in  I} U_i$ and functions
          $f_i \colon  U_i \to \mathbb{R}$,
          the function
            \begin{equation*}
            f: \left| \begin{array}{c c l} 
            U & \longrightarrow & \mathbb{R} \\
            x & \longmapsto &  \text{$f_i(x)$ if $x \in U_i$}
            \end{array} \right.
          \end{equation*}
          is well-defined by the compatibility condition on the $U_i$
          and is continuous, since it is continuous on an open cover.
      \end{enumerate}
  \end{enumerate}
\end{example}

% 1.13
\begin{definition}
  \label{def:morphism-of-sheaves}
  A morphism $\varphi\colon \mathcal{F}_1 \to \mathcal{F}_2$ of sheaves is
  a morphism of presheaves.
\end{definition}

\begin{notation}
  We denote
  \begin{itemize}
    \item
      $\PreSh_{\mathcat{C}}(X)$ for the category of presheaves of $\mathcat{C}$ on $X$.
    \item
      $\Sh_{\mathcat{C}}(X)$ for the category of sheaves of $\mathcat{C}$ on $X$.
    \item
      Omitting the index, $\Sh(X)$ will stand for the category $\Sh_{\Ab}(X)$,
      the category of sheaves of abelian groups.
  \end{itemize}
\end{notation}

\begin{remark}
  There is a forgetful functor $\Sh_{\mathcat{C}}(X) \to \PreSh_{\mathcat{C}(X)}$.
  By
  \autoref{def:morphism-of-sheaves},
  this functor is fully faithful.
\end{remark}

\begin{recall}
  Let $A$ be a ring (always commutative with $1$).
  Define the \vocab{spectrum} 
  \[
    \Spec A \coloneqq \set{ \mathfrak{p} \subset A \suchthat \text{$\mathfrak{p}$ prime ideal} } 
  \]
  as the set of prime ideals of $A$.
  The \vocab{Zariski topology} on $\Spec A$ is defined by
  the closed subsets
  $\mathcal{V}(\mathfrak{a}) \coloneqq \set{ \mathfrak{p} \in \Spec A \suchthat\mathfrak{a} \subset \mathfrak{p} } $.

  A basis of the topology is given by
  $D(\mathfrak{a}) \coloneqq \set{ \mathfrak{p} \in \Spec A \suchthat \mathfrak{a}\not\in \mathfrak{p} } $.
\end{recall}

% 1.16
\begin{definition}
  \label{def:structure-sheaf}
  Let $A$ be a ring and $X = \Spec A$.
  The \vocab{structure sheaf} $\mathcal{O}_X$
  on $X$ is defined by
  \[
    \mathcal{O}_X(U)
    \coloneqq
    \set{
      s\colon  U \to  \coprod_{\mathfrak{p} \in \Spec A} A_{\mathfrak{p}}
      \suchthat
      \text{$(i)$ and $(ii)$}
    }
    ,
  \]
  where
  \begin{enumerate}[(i)]
    \item For all $\mathfrak{p} \in U$, $s(\mathfrak{p}) \in  A_{\mathfrak{p}}$.
    \item For all $\mathfrak{p}\in U$, there is some $V \subset U$ and $a, b \in A$
      with $\mathfrak{p} \in  V \subset D(b)$ such that
      $s(q) = \frac{a}{b} \in A_{\mathfrak{q}}$ for all $\mathfrak{q} \in V$.
  \end{enumerate}
\end{definition}

\begin{remark}
  $\mathcal{O}_X$ is a sheaf of rings:
  \begin{itemize}
    \item
      $\mathcal{O}_X(U)$ is a ring with pointwise multiplication and addition.
    \item
      Checking the sheaf properties works in the same way as in the case of
      smooth functions.
  \end{itemize}
\end{remark}
