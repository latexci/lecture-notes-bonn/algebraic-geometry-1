%! TEX root = ../../master.tex
\lecture[]{Mo 17 Okt 2022}{Untitled}

With this result, we can now compute the stalks of 

% 2. 18
\begin{corollary}
  Let $X = \Spec A$.
  Then, for all $\mathfrak{p} \in \Spec A$,
  there is an isomorphism
  $\varphi _{\mathfrak{p}}\colon  A_{\mathfrak{p}} \to  \mathcal{O}_{X, \mathfrak{p}}$,
  such that for all $a \in A \setminus \mathfrak{p}$,
  the diagram
  \[
    \begin{tikzcd}
      A_{\mathfrak{a}}
      \ar[swap]{d}{}
      \ar{r}{\varphi _{\mathfrak{a}}}
      &
      \mathcal{O}_X(D(a))
      \ar{d}{}
      \\
      A_{\mathfrak{p}}
      \ar[swap]{r}{\varphi _{\mathfrak{p}}}
      &
      \mathcal{O}_{X, \mathfrak{p}}
    \end{tikzcd}
  \]
  commutes.
\end{corollary}

\begin{proof}
  Observe that
  \[
    \directlimit_{\mathfrak{a} \in A \setminus \mathfrak{p}} A_{\mathfrak{a}}
    \xrightarrow{\cong}
    A_{\mathfrak{p}}
  \]
  by checking that the universal properties are the same.
  Then, observe that
  \[
    \directlimit_{\mathfrak{p} \in D(a)} \mathcal{O}_X (D(a))
    \xrightarrow{\cong}
    \mathcal{O}_{X, \mathfrak{p}}
    ,
  \]
  which follows from the explicit description of the direct limit
  in \todoref, as the familiy of principal opens is cofinal in the family
  of all opens.
\end{proof}

\begin{oral}
  Note that we now know the following:
  The sheafification of any presheaf $\mathcal{F}$ on $\Spec A$,
  which is given by $A_{\mathfrak{a}}$ on $D(a)$
  is necessarily the structure sheaf.

  So, in some sense,
  \autoref{def:structure-sheaf} is the only
  possible choice of the structure sheaf.
\end{oral}


\section{Kernels, Cokernels, Exactness}

In this section, we always assume that (pre)sheafs are (pre)sheafs of abelian groups.
As usual, $X$ will denote a topological space.

% 3.1
\begin{definition}
  Let $\mathcal{F}$ be a presheaf on $X$.
  Then, a \vocab{sub(pre)sheaf} of $\mathcal{F}$ is a (pre)sheaf $\mathcal{G}$
  such that $\mathcal{G}(U) \subset \mathcal{F}(U)$ for all open $U\subset X$,
  such that the restriction maps are induced by $\mathcal{F}$.
\end{definition}

% no number
\begin{notation}
  We will write $\mathcal{G} \subset \mathcal{F}$ for this.
\end{notation}

\begin{trivial}
  In general, we could take a (pre)sheaf $\mathcal{G}$ on $X$,
  together with a morphism $ι \colon \mathcal{G} \to \mathcal{F}$
  such that $ι$ is monic on all opens.
\end{trivial}

% 3.2
\begin{definition}
  The \vocab{presheaf kernel} of $\varphi $ is the presheaf
  $\ker^{pre} (\varphi )$ defined by
  \[
    \ker^{pre}(\varphi )(U) \coloneqq \ker (\varphi (U))
    .
  \]
  Similarly, define the \vocab{presheaf image} $\im ^{pre} (\varphi )$
  and the \vocab{presheaf cokernel} $\coker ^{pre}(\varphi )(U)$
  as the presheafs given by the image and cokernel on the open sets,
  respectively.

  In each case, the restriction maps are induced by
  those of $\mathcal{F}$ or $\mathcal{G}$.
\end{definition}

\begin{lemma}
  Let $\mathcal{F}$ and $\mathcal{G}$ be sheaves.
  Then, the presheaf kernel $\ker^{pre}(\varphi )$ is a sheaf.
\end{lemma}

\begin{proof}
  Let $U \subset X$ open be covered by $U = \bigcup_{i \in  I} U_i$.
  Let $s_i \in \ker^{pre}(\varphi ) (U_i)$ such that
  $\frestriction{s_i}{U_{ij}} = \frestriction{s_j}{U_{ij}}$.
  Since $\mathcal{F}$ is a sheaf,
  there exists an $s\in \mathcal{F}(U)$ such that
  $\frestriction{s}{U_i} = s_i$.

  Since $\ker^{pre}(\varphi ) (U_i) = \ker (\varphi (U_i))$,
  we know that $\varphi (U_i) (s_i) = 0$.

  By commutativity, we know that
  \[
    \frestriction{\varphi (U)(s)}{U_i}
    =
    \varphi (U_i) (\frestriction{s}{U_i})
    =
    0
    .
  \]
  As $\mathcal{G}$ is a sheaf,
  we get that $\varphi (U) (s) = 0$,
  hence $s\in \ker ^{pre}(\varphi )(U)$.

  Furthermore, this $s$ is unique by the uniqueness property of $\mathcal{F}$.
\end{proof}

\begin{example}
  By exercise 4
  \todo{exercises !},
  $\im^{pre}(\varphi )$ and $\coker ^{pre}(\varphi )$
  are in general \emph{not} sheaves,
  even if $\mathcal{F}$ and $\mathcal{G}$ are:

  Let $\mathcal{F}$ be a presheaf of Abelian groups on $X$
  and $\mathcal{F}_x = 0$ for all $x\in X$,
  then $\mathcal{F}^+ = 0$.

  As we proved that $\coker^{pre}(\exp ) = 0$
  on a basis, we also deduce that all stalks are zero,
  hence $\coker (\exp ) = 0$.
  But $\coker(\exp ( \mathbb{C}^{\times})) \neq 0$,
  so that $\coker ^{pre}(\exp )$ is also nonzero.

  Hence in this case,
  the presheaf cokernel and the sheaf cokernel do not agree.
\end{example}

% 3.5
\begin{definition}
  Let $\varphi \colon \mathcal{F} \to \mathcal{G}$
  be a morphism of sheaves.
  Then, the
  \begin{enumerate}[1.]
    \item \vocab{sheaf kernel} is defined as $\ker^{pre}(\varphi )$.
    \item \vocab{sheaf cokernel} is defined as $(\coker^{pre}(\varphi ))^+$.
    \item \vocab{sheaf image} is defined as $(\im^{pre}(\varphi ))^+$
  \end{enumerate}
\end{definition}

\begin{lemma}
  Let $\varphi \colon \mathcal{F} \to \mathcal{G}$ be a morphism of sheaves.
  Then,
  \begin{enumerate}[h]
    \item $\ker (\varphi ) \to \mathcal{F}$ is the kernel in $\Sh(X)$.
    \item $\mathcal{G} \to \coker (\varphi )$ is the cokernel in $\Sh(X)$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  For the kernel,
  this means that for each commutative diagram of the form
  \[
  \begin{tikzcd}[column sep = tiny]
    &
    \mathcal{F}'
    \ar[dashed]{dl}[swap]{\exists !}
    \ar{d}
    \ar{dr}{0}
    \\
    \ker \varphi 
    \ar{r}
    &
    \mathcal{F}
    \ar{r}
    &
    \mathcal{G}
  \end{tikzcd}
  \]
  the dotted arrow exists and is unique.
  But this is the case,
  since this the case on all opens.

  For the cokernel,
  this means that for every commutative diagram of the form
  \[
    \begin{tikzcd}
      \mathcal{F}
      \ar{r}
      \ar{dr}[swap]{0}
      &
      \mathcal{G}
      \ar{d}
      \ar{r}
      &
      \coker^{pre}(\varphi )
      \ar{dl}[near start]{\psi}[swap]{\exists !}
      \ar{r}
      &
      \coker(\varphi )
      \ar[dashed]{dll}{\exists !}
      \\
      &
      \mathcal{G}'
      ,
    \end{tikzcd}
  \]
  the dotted arrow exists and is unique.
  But in the category of presheaves,
  $\psi $ already exists uniquely by construction.
  Hence by the universal property of sheafification,
  the dotted arrow also exists uniquely.
\end{proof}

\begin{proposition}
  \label{prop:characterization-monomorphisms-in-sh-x}
  Let $\varphi \colon \mathcal{F} \to \mathcal{G}$ be a morphism
  of sheaves.
  Then, the following are equivalent:
  \begin{enumerate}[e]
    \item $\varphi $ is a monomorphism in $\Sh(X)$
    \item $\ker \varphi  = 0$
    \item $\ker (\varphi (U)) = 0$ for every open $U\subset X$
    \item $\ker (\varphi _x) = 0$ for every $x\in X$
  \end{enumerate}
\end{proposition}

\begin{recall}
  A map $\varphi $
  is called a monomorphism if $\varphi  \circ  \psi  = \varphi  \circ  \psi '$,
  then $\psi  = \psi '$.
\end{recall}

\begin{proof}
  For $(1) \implies (2)$, apply the monomorphism property to
  $\ker \varphi  \to \mathcal{F}$.

  For $(2) \implies (1)$,
  if $\varphi  \circ \psi  = 0$,
  then $\psi$ factors through $\ker(\varphi ) \to \mathcal{F}$,
  hence $\psi  = 0$.

  For $(2) \iff (3)$, just use that $\ker (\varphi )(U) = \ker(\varphi (U))$.

  $(3) \implies (4)$ follows since $\directlimit$ is exact.
  \todoref.

  We now proof $(4) \implies (3)$.
  Let $s \in \mathcal{F}(U)$ with $\varphi (U)(s) = 0$.
  Then, $\varphi _x(s_u) = (\varphi (U)(s))_x = 0$ for all $x\in  U$.
  Hence, by injectivity of $\ker(\varphi _x)$,
  we deduce that $s_x = 0$ for all $x\in U$.
  Hence by the sheaf property, $s = 0$.
  So $\ker (\varphi (U)) = 0$ as claimed.
\end{proof}

% 3.8
\begin{proposition}
  \label{prop:characterization-epimorphisms-in-sh-x}
  Let $\varphi \colon \mathcal{F} \to \mathcal{G}$ be a morphism
  of sheaves.
  Then, the following are equivalent:
  \begin{enumerate}[e]
    \item $\varphi $ is an epimorphism in $\Sh(X)$
    \item $\coker \varphi  = 0$.
    \item $\coker \varphi _x = 0$ for all $x\in X$.
  \end{enumerate}
\end{proposition}

\begin{recall}
  A map $\varphi $ is called an epimorphism,
  if for every $\psi$, $\psi$,
  $\psi  \circ  \varphi  = \psi ' \circ  \varphi $
  implies $\psi  = \psi '$.
\end{recall}

\todo{remark on abelian categories}

\begin{proof}
  $(1) \implies (2)$.
  We directly show that $0$ has the universal property of the cokernel,
  by using that $G \to \mathcal{G}'$ is zero by the epimorphism property.

  $(2) \implies (3)$.
  We have
  \[
    0 = (\coker \varphi )_x
    =
    (\coker^{pre} \varphi )_x
    =
    \coker (\varphi _x)
  \]
  
  $(3) \implies (1)$.
  Let $\psi \colon \mathcal{G} \to \mathcal{G}'$ such that $\psi  \circ \varphi  = 0$.
  Thus, $0 = (\psi  \circ  \varphi )_x = \psi _x \circ \varphi _x$
  on stalks as well.
  But $\varphi _x$ is an epimorphism, so $\psi_x = 0$.

  Now, consider the $\Hom$-sheaf $\Hom(\mathcal{G}, \mathcal{G}')$,
  hence $\psi  = 0$, since all its stalks are zero.
\end{proof}

\begin{remark}
  While it is true that
  \[
    \forall U \subset X: \; \coker (\varphi (U)) = 0
    \quad
    \implies
    \quad
    \coker \varphi  = 0
    ,
  \]
  the converse is not true.
  \todo{smth smth cohomology}
\end{remark}

\begin{corollary}
  If $\varphi \colon \mathcal{F} \to \mathcal{G}$ is a morphism of sheaves,
  the following are equivalent:
  \begin{enumerate}[e]
    \item $\varphi $ is an isomorphism
    \item $\varphi (U)$ is an isomorphism for all open $U \subset X$
    \item $\varphi _x$ is an isomorphism for all $x\in X$
  \end{enumerate}
\end{corollary}

\begin{proof}
  $(1) \implies (2)$,
  since $(-)(U)$ is a functor.

  $(2) \implies (3)$, since $\directlimit$ is functorial.

  $(2) \implies(1)$, because $(\varphi (U)) ^{-1}$
  define a corresponding morphism of sheaves.

  The key part is now to show that $(3) \implies (2)$.
  We already know that $\varphi (U)$ is injective by
  \autoref{prop:characterization-monomorphisms-in-sh-x},
  so it remains to show surjectivity of $\varphi (U)$.

  So let $t\in \mathcal{G}(U)$.
  Since $\varphi _x$ is an isomorphism for all $x\in U$,
  we can find $s_x \in \mathcal{F}_x$ with $\varphi _x(s_x) = t_x$
  for all $x$.

  Since $(\varphi (V_x)(s_{V_x})) _x = t_x$,
  we can choose $V_x$ sufficiently small such that
  $\varphi (V_x)(s_{V_x}) = \frestriction{t}{V_x}$.

  Since $\varphi (V_x \cap V_y)$ is injective
  and
  \[
    \varphi (V_x \cap V_y)(\frestriction{s_{V_x}}{V_x \cap V_y}
    =
    \frestriction{t}{V_x \cap V_y}
    =
    \varphi (V_x \cap V_y)(\frestriction{s_{V_y}}{V_x \cap V_y})
    ,
  \]
  we deduce that the $\set{ s_{V_x} } $ are compatible,
  so that there exists an $s\in \mathcal{F}(U)$ such that
  $\frestriction{s}{V_x} = s_{V_x}$.

  But now, since
  \[
    \frestriction{\varphi (U)(s)}{V_x}
    =
    \varphi (V_x) (\frestriction{s}{V_x})
    =
    \frestriction{t}{V_x}
  \] 
\end{proof}

\begin{oral}
  The key ingredient of the proof was to lift isomorphisms on stalks
  to isomorphisms on open subsets.
  Surjectivity on stalks enables us to find
  local preimages of any section $t\in \mathcal{G}(U)$.
  The crucial part was that these are necessarily compatible,
  since $\varphi $ is also injective, which allows us to glue these local
  preimages.
\end{oral}

% recall remark 2.12
\begin{remark}
  For all $x$, there is some open $x \in V_x$
  along with $s_{V_x} \in \mathcal{F}(V_x)$,
  such that $(s_{V_x})_x = s_x$.
\end{remark}

% 3. 12
\begin{definition}
  A sequence of sheaves
  \[
    \begin{tikzcd}
      \mathcal{F}_1
      \ar{r}{\varphi _1}
      &
      \mathcal{F}_2
      \ar{r}{\varphi _2}
      &
      \mathcal{F}_3
    \end{tikzcd}
  \]
  is called \vocab{exact},
  if $\ker(\varphi_2) = \im (\varphi _1)$.
\end{definition}

\begin{remark}+
  In particular, a sequence
  \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      \mathcal{F}_1
      \ar{r}{\varphi _1}
      &
      \mathcal{F}_2
      \ar{r}{\varphi _2}
      &
      \mathcal{F}_3
      \ar{r}
      &
      0
    \end{tikzcd}
  \]
  is called exact if $\ker \varphi _1 = \coker \varphi _2 = 0$
  and $\ker \varphi _2 = \im \varphi _1$.
\end{remark}

\begin{corollary}
  A sequence of sheaves
  \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      \mathcal{F}_1
      \ar{r}{\varphi _1}
      &
      \mathcal{F}_2
      \ar{r}{\varphi _2}
      &
      \mathcal{F}_3
      \ar{r}
      &
      0
    \end{tikzcd}
  \]
  is exact if and only if
  \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      \mathcal{F}_{1, x}
      \ar{r}{\varphi _{1,x}}
      &
      \mathcal{F}_{2,x}
      \ar{r}{\varphi _2, x}
      &
      \mathcal{F}_{3,x}
      \ar{r}
      &
      0
    \end{tikzcd}
  \]
  is exact for all $x\in X$.
\end{corollary}

\begin{proof}
  Use that $\varphi _1$ is a monomorphism if and only if $\varphi _{1, x}$
  is a monomorphism for all $x\in X$
  and that $\varphi _{2}$ is an epimorphism if and only if
  $\varphi _{2, x}$ is an epimorphism for all $x\in X$.
\end{proof}
